var data;
 
function preload() {
    data = loadJSON("dev_first_sentence_as_candidates_temp.json");
}
 
function setup() {
    noLoop();
}
 
function draw() {
    background(200);
 
    data.sort(dynamicSort("query_id"));
    console.log(data);
}
 
 
function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}
